import random
import os
import colorama
import platform

from playsound import playsound
from colorama import Fore, Back, Style
from torpydo.ship import Color, Letter, Position, Ship
from torpydo.game_controller import GameController
from torpydo.telemetryclient import TelemetryClient

print("Starting")

myFleet = []
enemyFleet = []


def main():
    TelemetryClient.init()
    TelemetryClient.trackEvent('ApplicationStarted', {'custom_dimensions': {'Technology': 'Python'}})
    colorama.init()
    print(Fore.YELLOW + r"""
                                    |__
                                    |\/
                                    ---
                                    / | [
                             !      | |||
                           _/|     _/|-++'
                       +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
 __..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                        Welcome to Battleship                         BB-61/
 \_________________________________________________________________________|""" + Style.RESET_ALL)

    initialize_game()

    start_game()


def start_game():
    global myFleet, enemyFleet
    tabela = GameController.create_table(n=8,m=8)
    previous_moves_player = []
    # clear the screen
    if (platform.system().lower() == "windows"):
        cmd = 'cls'
    else:
        cmd = 'clear'
    os.system(cmd)
    print(r'''
                  __
                 /  \
           .-.  |    |
   *    _.-'  \  \__/
    \.-'       \
   /          _/
   |      _  /
   |     /_\
    \    \_/
     """"""""''')

    print("")
    while True:
        print()
        print(Fore.BLUE + "Player, it's your turn")
        print(Fore.YELLOW + '\nEnemy ship status:')
        GameController.print_ship_status(enemyFleet, 1)
        print(Fore.BLUE + "Possible moves")
        GameController.print_posible_moves_table(tabela, previous_moves_player)
        position = parse_position(input(Fore.BLUE + "Enter coordinates for your shot :"))
        previous_moves_player.append(position)
        is_hit = GameController.check_is_hit(enemyFleet, position)

        if is_hit:
            print(Fore.GREEN + r'''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''')

        print(Fore.GREEN + "\nYeah ! Nice hit !" if is_hit else Fore.RED + "\nMiss")

        print("")
        TelemetryClient.trackEvent('Player_ShootPosition',
                                   {'custom_dimensions': {'Position': str(position), 'IsHit': is_hit}})
        all_enemy_ships_sunk = [ship.is_sunk for ship in enemyFleet]
        if all(all_enemy_ships_sunk):

            print(Fore.YELLOW + r"""                                       .''.
                   .''.      .        *''*    :_\/_:     .
                  :_\/_:   _\(/_  .:.*_\/_*   : /\ :  .'.:.'.
              .''.: /\ :    /)\   ':'* /\ *  : '..'.  -=:o:=-
             :_\/_:'.:::.  | ' *''*    * '.\'/.'_\(/_'.':'.'
             : /\ : :::::  =  *_\/_*     -= o =- /)\    '  *
              '..'  ':::' === * /\ *     .'/.\'.  ' ._____
                  *        |   *..*         :       |.   |' .---"|
                    *      |     _           .--'|  ||   | _|    |
                    *      |  .-'|       __  |   |  |    ||      |
                 .-----.   |  |' |  ||  |  | |   |  |    ||      |
             ___'       ' /"\ |  '-."".    '-'   '-.'    '`      |____
            jgs~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
              &                    ~-~-~-~-~-~-~-~-~-~   /|
             ejm97    )      ~-~-~-~-~-~-~-~  /|~       /_|\
                    _-H-__  -~-~-~-~-~-~     /_|\    -~======-~
            ~-\XXXXXXXXXX/~     ~-~-~-~     /__|_\ ~-~-~-~
            ~-~-~-~-~-~    ~-~~-~-~-~-~    ========  ~-~-~-~""")

            print(Fore.GREEN + r"""  ___    ___  ________   ___  ___          ___       __    ___   ________           ___   ___   ___       
 |\  \  /  /||\   __  \ |\  \|\  \        |\  \     |\  \ |\  \ |\   ___  \        |\  \ |\  \ |\  \      
 \ \  \/  / /\ \  \|\  \\ \  \\\  \       \ \  \    \ \  \\ \  \\ \  \\ \  \       \ \  \\ \  \\ \  \     
  \ \    / /  \ \  \\\  \\ \  \\\  \       \ \  \  __\ \  \\ \  \\ \  \\ \  \       \ \  \\ \  \\ \  \    
   \/  /  /    \ \  \\\  \\ \  \\\  \       \ \  \|\__\_\  \\ \  \\ \  \\ \  \       \ \__\\ \__\\ \__\   
 __/  / /       \ \_______\\ \_______\       \ \____________\\ \__\\ \__\\ \__\       \|__| \|__| \|__|   
|\___/ /         \|_______| \|_______|        \|____________| \|__| \|__| \|__|           ___   ___   ___ 
\|___|/                                                                                  |\__\ |\__\ |\__\
                                                                                         \|__| \|__| \|__| """)


            playsound('youWin.wav')
            break
        position = get_random_position()
        print(Fore.YELLOW + 'Your ship status:')
        GameController.print_ship_status(myFleet, 2)
        is_hit = GameController.check_is_hit(myFleet, position)
        print()
        print(Fore.BLUE + f"Computer shoot in {str(position)} and {Fore.RED + 'hit your ship!' if is_hit else Fore.GREEN + 'miss'}")
        TelemetryClient.trackEvent('Computer_ShootPosition',
                                   {'custom_dimensions': {'Position': str(position), 'IsHit': is_hit}})
        if is_hit:
            print(Fore.RED + r'''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''')

        all_my_ships_sunk = [ship.is_sunk for ship in myFleet]
        if all(all_my_ships_sunk):


            print(Fore.RED + r"""  ___    ___ ________  ___  ___          ___       ________  ________  _______      
 |\  \  /  /|\   __  \|\  \|\  \        |\  \     |\   __  \|\   ____\|\  ___ \     
 \ \  \/  / | \  \|\  \ \  \\\  \       \ \  \    \ \  \|\  \ \  \___|\ \   __/|    
  \ \    / / \ \  \\\  \ \  \\\  \       \ \  \    \ \  \\\  \ \_____  \ \  \_|/__  
   \/  /  /   \ \  \\\  \ \  \\\  \       \ \  \____\ \  \\\  \|____|\  \ \  \_|\ \ 
 __/  / /      \ \_______\ \_______\       \ \_______\ \_______\____\_\  \ \_______\
|\___/ /        \|_______|\|_______|        \|_______|\|_______|\_________\|_______|
\|___|/                                                        \|_________|         
                                                                                    
                                                                                    """)
            playsound('youLose.wav')
            break
        print("")
        print("==========================================================")

def parse_position(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    position = Position(letter, number)

    return Position(letter, number)


def get_random_position():
    rows = 8
    lines = 8

    letter = Letter(random.randint(1, lines))
    number = random.randint(1, rows)
    position = Position(letter, number)

    return position


def initialize_game():
    initialize_myFleet()

    initialize_enemyFleet()


def initialize_myFleet():
    global myFleet

    myFleet = GameController.initialize_ships()

    print(Fore.BLUE + "Please position your fleet (Game board has size from A to H and 1 to 8) :")

    for ship in myFleet:
        print()
        print(Fore.GREEN + f"Please enter the positions for the {ship.name} (size: {ship.size})")

        for i in range(ship.size):
            position_input = input(Fore.RESET + f"Enter position {i + 1} of {ship.size} (i.e A3):")
            ship.add_position(position_input)
            TelemetryClient.trackEvent('Player_PlaceShipPosition', {
                'custom_dimensions': {'Position': position_input, 'Ship': ship.name, 'PositionInShip': i}})


def initialize_enemyFleet():
    global enemyFleet

    enemyFleet = GameController.initialize_ships()

    enemyFleet[0].positions.append(Position(Letter.B, 4))
    enemyFleet[0].positions.append(Position(Letter.B, 5))
    enemyFleet[0].positions.append(Position(Letter.B, 6))
    enemyFleet[0].positions.append(Position(Letter.B, 7))
    enemyFleet[0].positions.append(Position(Letter.B, 8))

    enemyFleet[1].positions.append(Position(Letter.E, 6))
    enemyFleet[1].positions.append(Position(Letter.E, 7))
    enemyFleet[1].positions.append(Position(Letter.E, 8))
    enemyFleet[1].positions.append(Position(Letter.E, 9))

    enemyFleet[2].positions.append(Position(Letter.A, 3))
    enemyFleet[2].positions.append(Position(Letter.B, 3))
    enemyFleet[2].positions.append(Position(Letter.C, 3))

    enemyFleet[3].positions.append(Position(Letter.F, 8))
    enemyFleet[3].positions.append(Position(Letter.G, 8))
    enemyFleet[3].positions.append(Position(Letter.H, 8))

    enemyFleet[4].positions.append(Position(Letter.C, 5))
    enemyFleet[4].positions.append(Position(Letter.C, 6))


if __name__ == '__main__':
    main()
