import random

from colorama import Fore

from torpydo.ship import Color, Letter, Position, Ship


class GameController(object):
    def check_is_hit(ships: list, shot: Position):
        if ships is None:
            raise ValueError('ships is null')

        if shot is None:
            raise ValueError('shot is null')

        for ship in ships:
            for position in ship.positions:
                if position == shot:
                    ship.add_hit_position(shot)
                    return True

        return False

    def initialize_ships():
        return [
            Ship("Aircraft Carrier", 5, Color.CADET_BLUE),
            Ship("Battleship", 4, Color.RED),
            Ship("Submarine", 3, Color.CHARTREUSE),
            Ship("Destroyer", 3, Color.YELLOW),
            Ship("Patrol Boat", 2, Color.ORANGE)]

    def is_ship_valid(ship: Ship):
        is_valid = len(ship.positions) == ship.size

        return is_valid

    def get_random_position(size: int):
        letter = random.choice(list(Letter))
        number = random.randrange(size)
        position = Position(letter, number)

        return position

    def print_ship_status(fleet, player):
        good_color = Fore.GREEN
        bad_color = Fore.RED
        if player == 1:
            good_color = Fore.RED
            bad_color = Fore.GREEN
        for ship in fleet:
            status = good_color + 'Still in game!'
            if ship.is_sunk:
                status = bad_color + 'Sunk!'

            print(Fore.YELLOW + f'\tName: {ship.name}, Size: {ship.size}, Status: {status}')

    def create_table(n, m):
        tabela = []
        letters = {1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H'}
        for i in range(n):
            for j in range(m):
                l = Letter[letters[i+1].upper()[:1]]
                tabela.append(Position(l, j + 1))

        return tabela

    def print_posible_moves_table(tabela, moves_player):
        list_print = []
        for tp in tabela:
            position_was = 0
            for mp in moves_player:
                if tp == mp:
                    position_was = 1

            if position_was:
                continue
            list_print.append(tp)
        print('\n' + Fore.GREEN + str(list_print) + '\n')


